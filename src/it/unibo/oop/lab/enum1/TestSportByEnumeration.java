package it.unibo.oop.lab.enum1;


import it.unibo.oop.lab.socialnetwork.User;


/**
 * This is going to act as a test for
 * {@link it.unibo.oop.lab.enum1.SportSocialNetworkUserImpl}
 * 
 * 1) Realize the same test as the previous exercise, this time using
 * enumeration Sport
 * 
 * 2) Run it: every test must return true.
 * 
 */
public final class TestSportByEnumeration {

    private TestSportByEnumeration() {
    }

    /**
     * @param args
     *            ignored
     */
    public static void main(final String... args) {
        /*
         * [TEST DEFINITION]
         * 
         * By now, you know how to do it
         */
        // TODO
    	/*
    	 * add 3 users
    	 */
    	final SportSocialNetworkUserImpl<User> dcassani = new SportSocialNetworkUserImpl<>("Davide", "Cassani", "dcassani", 53);
        final SportSocialNetworkUserImpl<User> becclestone = new SportSocialNetworkUserImpl<>("Bernie", "Ecclestone", "becclestone", 83);
        final SportSocialNetworkUserImpl<User> falonso = new SportSocialNetworkUserImpl<>("Fernando", "Alonso", "falonso", 34);
        
        /*
         * fill the users with some sports
         */
        dcassani.addSport(Sport.BIKE);
        falonso.addSport(Sport.F1);
        falonso.addSport(Sport.BASKET);
        falonso.addSport(Sport.SOCCER);
        becclestone.addSport(Sport.F1);
        becclestone.addSport(Sport.TENNIS);
        
        /*check if 
         * some users like / dislike a certain sport
         */
        
        System.out.println(dcassani.hasSport(Sport.BASKET));    //false
        System.out.println(falonso.hasSport(Sport.SOCCER));    //true
        System.out.println(falonso.hasSport(Sport.F1));        //true
        System.out.println(becclestone.hasSport(Sport.TENNIS)); //true
    }

}
